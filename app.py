from flask import Flask
from flask_graphql import GraphQLView
from src.schema import schema
import src.connection
import src.config
import os

port = os.environ.get('PORT')
app = Flask(__name__)
app.debug = False

app.add_url_rule(
    "/graphql", view_func=GraphQLView.as_view("graphql", schema=schema, graphiql=True)
)


if __name__ == "__main__":
  app.run(debug=False, port=port)

