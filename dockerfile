# syntax=docker/dockerfile:1
FROM python:3.9.7
ADD . /nlp-server
WORKDIR /nlp-server

# RUN apk add --no-cache --update \
#     python3 python3-dev gcc \
#     gfortran musl-dev g++ \
#     libffi-dev openssl-dev \
#     libxml2 libxml2-dev \
#     libxslt libxslt-dev \
#     libjpeg-turbo-dev zlib-dev && \
#     ln -s /usr/include/locale.h /usr/include/xlocale.h && \
RUN   pip3 install -U pip && \ 
    pip3 install -U setuptools

COPY requirements.txt requirements.txt
RUN pip3 install --no-cache-dir -r requirements.txt
EXPOSE 5001
COPY . .

# CMD [ "pip3", "list"]
CMD [ "python3", "app.py"]
# CMD [ "python", "-m" , "flask", "run"]

