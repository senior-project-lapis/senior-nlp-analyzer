pymongo[srv]
Flask
Flask-GraphQL
graphene-mongo
mongomock
python-dotenv
nltk
contractions
numpy
pandas

simpletransformers
spacy
gensim
scikit-learn
torch
graphene_federation 
en-core-web-sm @ https://github.com/explosion/spacy-models/releases/download/en_core_web_sm-3.2.0/en_core_web_sm-3.2.0-py3-none-any.whl