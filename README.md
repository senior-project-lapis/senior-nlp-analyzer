
Flask+MongoEngine Senior Project
================================

Getting started
---------------

First you'll need to get the source of the project. Do this by cloning the
whole Graphene repository:

```bash
$ git clone git@gitlab.com:senior-project-lapis/senior-nlp-analyzer.git
$ cd senior-nlp-analyzer
```

It is good idea (but not required) to create a virtual environment
for this project. We'll do this using
[virtualenv](http://docs.python-guide.org/en/latest/dev/virtualenvs/)
to keep things simple,
but you may also find something like
[virtualenvwrapper](https://virtualenvwrapper.readthedocs.org/en/latest/)
to be useful:

```bash
$ virtualenv env

# For Mac
$ source env/bin/activate

# For window
$ source env/Script/activate
```

Now we can install our dependencies:

```bash
$ pip install -r requirements.txt
```

Befor start the server you must 
``
add ".env" file to main path.
``Then, start the server:

```bash
$ python app.py
```
Or, run with docker
---------------

First, build docker image

```bash
$ docker build --tag senior-nlp .
```

Run container with port mapping
```bash
$ docker run -d -p 5000:5000 senior-nlp
```

Check running container
```bash
$ docker ps
```

Now head on over to
[http://127.0.0.1:5000/graphql](http://127.0.0.1:5000/graphql)
and run some queries!

Sample query:
```
{
  reviews {
    Id
    title
    createAt
    updateAt
    companyId
    pro
    con
    topic
    sentiment
  }
}
```
