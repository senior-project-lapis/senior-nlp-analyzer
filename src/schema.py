from unittest import skip
import graphene
import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

from .mutations.review_mutation import CreateReviewMutation
from .models.review_model import Review as ReviewModel
from .models.job_model import Job as JobModel
from .dtos.review_dto import ReviewDto
from .dtos.job_dto import JobDto
from graphql.utils import schema_printer
from .inputs.job_input import JobInputType
import timeit

from graphene_federation import build_schema, key

global_job_query_set = None

class Query(graphene.ObjectType):
    reviews = graphene.List(ReviewDto)
    def resolve_reviews(self, info):
        return list(ReviewModel.objects.all())
    
    JobsByProfile = graphene.List(JobDto, user=graphene.Argument(JobInputType))
    def resolve_JobsByProfile(self, info, user):
        start = timeit.default_timer()
        global global_job_query_set

        if global_job_query_set == None:
            global_job_query_set = JobModel.objects.all()
        job_df = pd.DataFrame.from_records(global_job_query_set.values_list(
            '_id',
            'sourceType',
            'sourceId',
            'contents',
            'name',
            'shortName',
            'jobType',
            'locations',
            'categories',
            'experienceLevels',
            'companyId',
            'companyName',
            'applyUrl',
            'salary',
            'remote',
            'benefits',
            'requirements',
            'description',
            'educationLevel',
            'status',
            'createAt',
            'updateAt'
            ),
            columns=[
                '_id',
                'sourceType',
                'sourceId',
                'contents',
                'name',
                'shortName',
                'jobType',
                'locations',
                'categories',
                'experienceLevels',
                'companyId',
                'companyName',
                'applyUrl',
                'salary',
                'remote',
                'benefits',
                'requirements',
                'description',
                'educationLevel',
                'status',
                'createAt',
                'updateAt'
            ]
        )
        documents = documents = job_df['contents'].values.tolist()
        base_document = user_to_content(user)
        sorted_jobs = process_tfidf_similarity(documents, base_document, job_df)
        stop = timeit.default_timer()
        print('Time: ', stop - start)
        result_data = {
            'jobResult': sorted_jobs[skip:limit],
            'totalCount': len(sorted_jobs)
        } 
        return sorted_jobs[skip:limit]

class Mutation(graphene.ObjectType):
    CreateReview = CreateReviewMutation.Field()

def user_to_content(user):
    if len(user['experience']) > 0:
        result = "requirement "
        for experience in user['experience']:
            result = result + f"{experience['name']} {experience['year']} year "

    if len(user['skill']) > 0:
        result = result + "skill "
        for skill in user['skill']:
            result = result + f"{skill['name']} "
        result = result + "education "

    if len(user['education']) > 0:
        for education in user['education']:
            result = result + f"{education['educationLevel']} "

    return result.lower()

def process_tfidf_similarity(documents, base_document, job_df):
    stopword = stopwords.words('english')
    vectorizer = TfidfVectorizer(max_features=2000,stop_words=stopword, lowercase=True)

    documents.insert(0, base_document)
    embeddings = vectorizer.fit_transform(documents)
    scores = cosine_similarity(embeddings[0], embeddings[1:]).flatten()
    job_df['cosine_similarities'] = scores
    data = [JobModel(
        _id = item['_id'],
        sourceType = item['sourceType'],
        sourceId = item['sourceId'],
        contents = item['contents'],
        name = item['name'],
        shortName = item['shortName'],
        jobType = item['jobType'],
        locations = item['locations'],
        categories = item['categories'],
        experienceLevels = item['experienceLevels'],
        companyId = item['companyId'],
        companyName = item['companyName'],
        applyUrl = item['applyUrl'],
        salary = item['salary'],
        remote = item['remote'],
        benefits = item['benefits'],
        requirements = item['requirements'],
        description = item['description'],
        educationLevel = item['educationLevel'],
        status = item['status'],
        createAt = item['createAt'],
        updateAt = item['updateAt']
        ) for item in job_df.sort_values(by=['cosine_similarities'], ascending=False).drop(columns=['cosine_similarities']).to_dict('records')]
    return data

schema = build_schema(query=Query, mutation=Mutation, types=[ReviewDto, JobDto], auto_camelcase=False)

my_schema_str = schema_printer.print_schema(schema)
fp = open("schema.gql", "w")
fp.write(my_schema_str)
fp.close()