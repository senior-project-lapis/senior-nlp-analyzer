import spacy
import pickle
import contractions
import pandas as pd
from nltk.corpus import stopwords
import regex as re
from gensim.models import Phrases
from gensim.models.phrases import Phraser
from nltk.corpus import stopwords
import graphene

from ..dtos.review_dto import ReviewDto
from ..models.review_model import Review as ReviewModel
from ..models.company_model import CompanyProperty
from ..models.company_model import Company as CompanyModel
from ..inputs.review_input import ReviewInputType
import logging
import torch
import numpy as np
from simpletransformers.classification import ClassificationModel, ClassificationArgs

sentiment_model = None
# python -m spacy download en_core_web_sm

def clean_text(text):
    REPLACE_BY_SPACE_RE = re.compile('[/(){}\[\]\|@,;]')
    BAD_SYMBOLS_RE = re.compile('[^0-9a-z \-#+_]')
    REMOVE_NUMERIC_SEQUENCE = re.compile('\d+')
    
    text = text.lower()
    text = REPLACE_BY_SPACE_RE.sub(' ', text)
    text = BAD_SYMBOLS_RE.sub('', text)
    text = REMOVE_NUMERIC_SEQUENCE.sub('', text)
    return text

def tokenize(text):
    return re.findall(r'[\w-]*\p{L}[\w-]*', text)

def remove_stopwords(review):
    stop_words = stopwords.words('english')
    stop_words.extend(['from', 'subject', 're', 'edu', 'use'])
    return [[word for word in doc if word not in stop_words] for doc in review]
                
def make_trigram(texts):
    bigram = Phrases(texts, min_count=5, threshold=100)
    trigram = Phrases(bigram[texts], threshold=100)
    bigram_mod = Phraser(bigram)
    trigram_mod = Phraser(trigram)
    return [trigram_mod[bigram_mod[doc]] for doc in texts]
                                
def lemmatization(texts, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV']):
    nlp = spacy.load('en_core_web_sm', disable=['parser', 'ner'])
    texts_out = []
    for sent in texts:
        doc = nlp(" ".join(sent)) # Adds English dictionary from Spacy
        texts_out.append([token.lemma_ for token in doc if token.pos_ in allowed_postags])
    return texts_out

def get_sentiment_model():
    global sentiment_model
    if sentiment_model is None:
        model_args = {"labels_list" : [0, 1], "use_multiprocessing": False, "use_multiprocessing_for_evaluation": False, "process_count": 1}
        sentiment_model = ClassificationModel("roberta", "src/predictions/sentiment/best_model", use_cuda=torch.cuda.is_available(),args=model_args)
    return sentiment_model

def get_topic_model():
    with open(r"src/predictions/senior_lda_model_3_cut_29k.pickle", "rb") as pkl:
        loaded_model = pickle.load(pkl)
        return loaded_model

def get_id2word():
    with open(r"src/predictions/id2word.pickle", "rb") as pkl:
        id2word = pickle.load(pkl) 
        return id2word  

def predict_sentiment(text):
    model = get_sentiment_model()
    predictions, raw_outputs = model.predict([text])
    if predictions[0] == 1:
        return "positive"
    elif predictions[0] == 0:
        return "negative"
    return None

def predict_topic(text):
    # Load model 3 topic
    loaded_model = get_topic_model()
    # Load id2word
    id2word = get_id2word()
    
    labels = ['Problem & Support', 'Work & Management', 'Time']
    max_val = 0
    best_topic = -1
    cleaned_data = clean_text(text)
    token_data = tokenize(cleaned_data)
    removed_contract = [contractions.fix(word) for word in token_data]
    removed_stop = remove_stopwords([removed_contract])
    trigram_data = make_trigram(removed_stop)
    lemmatized_data = lemmatization(trigram_data)
    new_text_corpus = id2word.doc2bow(lemmatized_data[0])
    predicted_topic = loaded_model[new_text_corpus][0]
    
    for topic, value in predicted_topic:
        if value > max_val:
            max_val = value
            best_topic = topic
    return labels[best_topic]

class CreateReviewMutation(graphene.Mutation):
    review = graphene.Field(ReviewDto)

    class Arguments:
        review_data = ReviewInputType(required=True)

    def mutate(self, info, review_data=None):
        # Process sentiment & topic modeling
        topic = predict_topic(review_data.content)
        sentiment = predict_sentiment(review_data.content)
        review = ReviewModel(
            companyId = review_data.companyId,
            authorPosition = review_data.authorPosition,
            title = review_data.title,
            content = review_data.content,
            pro = review_data.pro,
            con = review_data.con,
            topic = topic,
            sentiment = sentiment
        )
        review.save()
        
        #Company part
        review_in_company = ReviewModel.objects.filter(companyId=review_data.companyId)
        review_df = pd.DataFrame.from_records(review_in_company.values_list(
                '_id',
                'companyId',
                'authorPosition',
                'title',
                'score',
                'content',
                'pro',
                'con',
                'topic',
                'sentiment',
                'createAt',
                'updateAt', 
            ),
            columns=[
                '_id',
                'companyId',
                'authorPosition',
                'title',
                'score',
                'content',
                'pro',
                'con',
                'topic',
                'sentiment',
                'createAt',
                'updateAt',   
            ]
        )

        topics_count = review_df['topic'].value_counts()
        sentiment_count = (review_df.groupby(['topic'])['sentiment'].value_counts())

        properties = list()
        for topic in topics_count.keys():
            topic_stat = (sentiment_count[topic] * 100 / topics_count[topic])
            properties.append(
                CompanyProperty(
                    name=topic,
                    positive=topic_stat.get('positive', 0),
                    negative=topic_stat.get('negative', 0)
                )
            )
        company = CompanyModel.objects.get(_id=review_data.companyId)
        company['properties'] =  properties
        company.save()
        return CreateReviewMutation(review=review)
    