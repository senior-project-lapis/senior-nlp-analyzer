from mongoengine import connect
from mongoengine.connection import disconnect
import os

database_name = os.environ.get('DATABASE_NAME')
database_host = os.environ.get('DATABASE_HOST')

disconnect(alias='default')
connect(database_name, host=database_host, alias="default")
