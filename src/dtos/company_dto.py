from graphene_mongo import MongoengineObjectType
from ..models.company_model import Company as CompanyModel
from graphene_federation import build_schema, key

@key(fields='_id')
class CompanyDto(MongoengineObjectType):
    class Meta:
        model = CompanyModel