import graphene
from graphene_mongo import MongoengineObjectType
from ..models.job_model import Job as JobModel
from graphene_federation import build_schema, key

@key(fields='_id')
class JobDto(MongoengineObjectType):
    class Meta:
        model = JobModel

# class JobObjectType(graphene.ObjectType):
#     _id = graphene.ID()
#     sourceId = graphene.String()
#     contents = graphene.String()
#     name = graphene.String()
#     shortName = graphene.String()
#     jobType = graphene.Enum('jobType', [('external', 'external'), ('internal', 'internal')])
#     locations = graphene.List(graphene.String)
#     categories = graphene.List(graphene.String)
#     experienceLevels = graphene.List(graphene.Enum('experienceLevels', [('internship', 'internship'), ('entry', 'entry'), ('mid', 'mid'), ('senior', 'senior'), ('management', 'management')]))
#     companyId = graphene.String()
#     companyName = graphene.String()
#     applyUrl = graphene.String()
#     salary = graphene.String()
#     remote = graphene.Boolean()
#     benefits = graphene.List(graphene.String)
#     requirements = graphene.List(graphene.String)
#     description = graphene.String()
#     educationLevel = graphene.String()
#     status =  graphene.List(graphene.Enum('status', [('normal', 'normal'), ('reported', 'reported'), ('banned', 'banned')]))
#     createAt = graphene.DateTime()
#     updateAt = graphene.DateTime()

# class JobResultDto(graphene.ObjectType):
#     JobResult = graphene.List(JobObjectType)
#     totalCount = graphene.Int()