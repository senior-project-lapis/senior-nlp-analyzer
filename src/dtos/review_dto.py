from graphene_mongo import MongoengineObjectType
from ..models.review_model import Review as ReviewModel
from graphene_federation import build_schema, key

@key(fields='_id')
class ReviewDto(MongoengineObjectType):
    class Meta:
        model = ReviewModel