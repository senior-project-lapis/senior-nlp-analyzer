from datetime import datetime
from mongoengine import Document
from bson.objectid import ObjectId
from mongoengine.fields import (
    DateTimeField,
    StringField,
    ObjectIdField,
    FloatField,
    EmbeddedDocumentListField,
    ListField,
    EmbeddedDocument
)

SourceTypeEnum = {'external': 'external','internal': 'internal'}
CompanySizeEnum = {'small': 'small', 'medium': 'medium', 'large': 'large',}
CompanyStatusEnum = {'approved': 'approved', 'pending': 'pending',}

class CompanyProperty(EmbeddedDocument):
    name = StringField(required=True)
    positive = FloatField(required=True)
    negative = FloatField(required=True)

class Company(Document):
    meta = {"collection": "companies"}

    _id = ObjectIdField(default=ObjectId)
    sourceType = StringField(choices=SourceTypeEnum.keys(), required=True)
    sourceId = StringField(required=False)
    description = StringField(required=False)
    locations = ListField(StringField(), required=True)
    industries = ListField(StringField(), required=True)
    shortName = StringField(required=True)
    name = StringField(required=True)
    size = StringField(choices=CompanySizeEnum.keys(), required=True)
    logoImage = StringField(required=False)
    properties = EmbeddedDocumentListField(CompanyProperty, required=False)
    status = StringField(choices=CompanyStatusEnum.keys(), required=True)

    createAt = DateTimeField(default=datetime.now)
    updateAt = DateTimeField(default=datetime.now)