from datetime import datetime
from mongoengine import Document
from bson.objectid import ObjectId
from mongoengine.fields import (
    DateTimeField,
    StringField,
    ObjectIdField,
    FloatField
)

SentimentEnum = {'positive': 'positive','negative': 'negative'}


class Review(Document):
    meta = {"collection": "reviews"}
    
    _id = ObjectIdField(default=ObjectId)
    companyId = StringField(required=True)
    authorPosition = StringField(required=True)
    clean_content = StringField(required=False)
    title = StringField(required=False)
    score = FloatField(required=False)
    content = StringField(required=True)
    pro = StringField(required=False)
    con = StringField(required=False)
    topic = StringField(required=False)
    sentiment = StringField(choices=SentimentEnum.keys(), required=False)
    createAt = DateTimeField(default=datetime.now)
    updateAt = DateTimeField(default=datetime.now)