from datetime import datetime
from mongoengine import Document
from bson.objectid import ObjectId
from mongoengine.fields import (
    DateTimeField,
    StringField,
    ObjectIdField,
    ListField,
    BooleanField
)

SourceTypeEnum = {'positive': 'positive','negative': 'negative'}
JobTypeEnum = {'external': 'external', 'internal': 'internal'}
JobStatusEnum = {'normal': 'normal', 'reported': 'reported', 'banned': 'banned'}
ExperienceLevelEnum = {'internship': 'internship','entry': 'entry','mid': 'mid', 'senior': 'senior', 'management': 'management'}

class Job(Document):
    meta = {"collection": "jobs"}
    
    _id = ObjectIdField(default=ObjectId)
    sourceType = StringField(choices=SourceTypeEnum.keys(), required=True)
    sourceId = StringField(required=False)
    contents = StringField(required=False)
    name = StringField(required=True)
    shortName = StringField(required=True)
    jobType = StringField(choices=JobTypeEnum.keys(), required=True)
    locations = ListField(StringField(), required=True)
    categories = ListField(StringField(), required=False)
    experienceLevels = ListField(StringField(choices=ExperienceLevelEnum.keys()), requited=True)
    companyId = StringField(required=True)
    companyName = StringField(required=True)
    applyUrl = StringField(required=False)
    salary = StringField(required=False)
    remote = BooleanField(required=True)
    benefits = ListField(StringField(), required=False)
    requirements = ListField(StringField(), required=False)
    description = StringField(required=False)
    educationLevel = StringField(required=False)
    status = StringField(choices=JobStatusEnum.keys(), required=True)
    createAt = DateTimeField()
    updateAt = DateTimeField()