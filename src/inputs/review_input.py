import graphene
class ReviewInputType(graphene.InputObjectType):
    companyId = graphene.String()
    authorPosition = graphene.String()
    title = graphene.String()
    content = graphene.String()
    pro = graphene.String(required=False)
    con = graphene.String(required=False)