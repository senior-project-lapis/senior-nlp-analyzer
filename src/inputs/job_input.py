from email.policy import default
from unittest import skip
import graphene
class ExperienceType(graphene.InputObjectType):
    name = graphene.String()
    year = graphene.Int()

class SkillType(graphene.InputObjectType):
    name = graphene.String()

class EducationType(graphene.InputObjectType):
    school = graphene.String()
    graduationYear = graphene.Int()
    educationLevel = graphene.String()

class JobInputType(graphene.InputObjectType):
    experience = graphene.List(ExperienceType)
    skill = graphene.List(SkillType)
    education = graphene.List(EducationType)
    skip = graphene.Int(default=0, required=False)
    limit = graphene.Int(default=1000, required=False)

# class Pagination(graphene.InputObjectType):
#     skip = graphene.Int(default=0)
#     limit = graphene.Int(default=1000)